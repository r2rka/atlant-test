import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    card_data: [],
    BTCSummary: 0,
    BTCTransactions: []
  },
  mutations: {
    setCardData(state, params) {
      state.card_data = params;
    },
    arrangeCardLayers(state, index) {
      /** Arranging Card layers to pull active on top and offset those in between */

      let z_values = state.card_data.map(card => {
        return card.z;
      });
      let origin_value = state.card_data[index].z;
      state.card_data[index].z =
        state.card_data[z_values.indexOf(Math.max(...z_values))].z;
      z_values.forEach((v, i) => {
        if (v > origin_value) state.card_data[i].z -= 1;
      });
    },
    setActiveCard(state, index) {
      state.work_space.selectedCardIndex = index;
    },
    setGlobalPointer(state, params) {
      state[params.name] = params.pointer;
    },
    setTransactionData(state, data) {
      state.BTCSummary += data.sum;
      state.BTCTransactions.push({
        from: data.from,
        to: data.to,
        sum: data.sum
      });
    },
    resetTransactions(state) {
      state.BTCSummary = 0;
      state.BTCTransactions = [];
    }
  },
  getters: {
    getCardData: state => {
      return state.card_data;
    },
    getBTCSummary: state => {
      return state.BTCSummary.toFixed(10);
    },
    getBTCTransactions: state => {
      return state.BTCTransactions;
    }
  },
  actions: {
    storeCards: ({ commit }, params) => {
      commit("setCardData", params);
    },
    addGlobalPointer: ({ commit }, params) => {
      commit("setGlobalPointer", params);
    }
  }
});
